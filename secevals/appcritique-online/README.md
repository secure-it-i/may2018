# Tool Setup

Online tool so need for setup

#Reproduce experiment

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

For each apk in ghera-apks upload the apk to [AppCritique server](https://appcritique.boozallen.com/upload) and download the report

#Results

See the evaluation results in *output/<config>/<benchmark>/AppCritiqueReport.pdf*

#References

[AppCritique](https://appcritique.boozallen.com/)
