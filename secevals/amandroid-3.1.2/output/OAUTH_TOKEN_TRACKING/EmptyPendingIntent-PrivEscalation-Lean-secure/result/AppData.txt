Application Name: EmptyPendingIntent-PrivEscalation-Lean-secure.apk
Uses Permissions: 

Component edu.cs.ksu.benign.SenitiveActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.cs.ksu.benign.MySensitiveService
  Component type: service
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["santos.cs.ksu.sensitiveServ"])

  Inter-component communication (ICC) Result:


Component edu.cs.ksu.benign.MyService
  Component type: service
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["santos.cs.ksu.myServ"])

  Inter-component communication (ICC) Result:


Component edu.cs.ksu.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L10a5a4)(MainActivity.envMain,L71)
      Outgoing Intents via this call:
        Intent:
          Actions:
            santos.cs.ksu.edu.benignpireceiver
          Explicit: false
          Precise: true


Taint analysis result:
  Sources found:
    <Descriptors: api_source: Landroid/app/PendingIntent;.getService:(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;>
  Sinks found:
    <Descriptors: api_sink: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
  Discovered taint paths are listed below:
    TaintPath:
      Source: <Descriptors: api_source: Landroid/app/PendingIntent;.getService:(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;>
      Sink: <Descriptors: api_sink: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V 1>
      Types: maliciousness:information_theft
      The path consists of the following edges ("->"). The nodes have the context information (p1 to pn means which parameter). The source is at the top :
        List(Call@(1.onClick,L10a570)(MainActivity.envMain,L71), Call@(1.onClick,L10a5a4)(MainActivity.envMain,L71) param: 1)