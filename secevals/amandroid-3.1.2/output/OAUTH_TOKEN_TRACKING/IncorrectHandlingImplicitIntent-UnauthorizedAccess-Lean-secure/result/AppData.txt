Application Name: IncorrectHandlingImplicitIntent-UnauthorizedAccess-Lean-secure.apk
Uses Permissions: 

Component edu.ksu.cs.benign.SensitiveActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: edu.ksu.cs.secure.perm
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.SENS_ACTIVITY_ACTION"],Categories:["android.intent.category.DEFAULT"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V
      Caller Context: (1.onClick,L0f3ed4)(MainActivity.envMain,L44)
      Outgoing Intents via this call:
        Intent:
          Actions:
            edu.ksu.cs.benign.SENS_ACTIVITY_ACTION
          Explicit: false
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.SensitiveActivity


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/app/Activity;.setResult:(ILandroid/content/Intent;)V 2>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V 1>
  Discovered taint paths are listed below:
