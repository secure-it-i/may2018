Application Name: SQLlite-SQLInjection-Lean-secure.apk
Uses Permissions: 

Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MiddleActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.DB"],Categories:["android.intent.category.DEFAULT"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MyContentProvider
  Component type: provider
  Exported: false
  Dynamic Registered: false
  Required Permission: edu.ks.cs.benign.MYCP_READ_PERMISSION
  IntentFilters:

  Inter-component communication (ICC) Result:



Taint analysis result:
  Sources found:
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MainActivity;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MiddleActivity;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MainActivity;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MiddleActivity;.envMain:(Landroid/content/Intent;)V>
  Sinks found:
  Discovered taint paths are listed below:
