Application Name: WeakPermission-UnauthorizedAccess-Lean-secure.apk
Uses Permissions: 

Component edu.ksu.cs.benign.MyContentProvider
  Component type: provider
  Exported: true
  Dynamic Registered: false
  Required Permission: edu.ksu.cs.benign.MYCP_ACCESS_PERM
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:



Taint analysis result:
  Sources found:
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MyContentProvider;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MyContentProvider;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MainActivity;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MainActivity;.envMain:(Landroid/content/Intent;)V>
  Sinks found:
  Discovered taint paths are listed below:
