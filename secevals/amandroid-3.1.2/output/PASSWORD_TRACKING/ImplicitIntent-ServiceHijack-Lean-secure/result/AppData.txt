Application Name: ImplicitIntent-ServiceHijack-Lean-secure.apk
Uses Permissions: 

Component edu.cs.ksu.benign.MyService
  Component type: service
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (MyService.onStartCommand,L0f150a)(MyService.env,L6)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.cs.ksu.benign.MainActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.cs.ksu.benign.MainActivity

Component edu.cs.ksu.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (1.onClick,L0f1492)(MainActivity.envMain,L34)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.cs.ksu.benign.MyService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.cs.ksu.benign.MyService


Taint analysis result:
  Sources found:
  Sinks found:
  Discovered taint paths are listed below:
