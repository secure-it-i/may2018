Application Name: NoValidityCheckOnBroadcastMsg-UnintendedInvocation-Lean-secure.apk
Uses Permissions: 

Component edu.ksu.cs.benign.DeleteStatusActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.DeleteFilesIntentService
  Component type: service
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (DeleteFilesIntentService.onHandleIntent,L0f175a)(DeleteFilesIntentService.env,L30)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.DeleteStatusActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.DeleteStatusActivity
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (DeleteFilesIntentService.onHandleIntent,L0f16a6)(DeleteFilesIntentService.env,L30)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.DeleteStatusActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.DeleteStatusActivity

Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.LowMemoryReceiver
  Component type: receiver
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.DEVICE_STORAGE_LOW"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (LowMemoryReceiver.onReceive,L0f1808)(LowMemoryReceiver.envMain,L66)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.DeleteFilesIntentService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.DeleteFilesIntentService
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (LowMemoryReceiver.onReceive,L0f17e2)(LowMemoryReceiver.envMain,L66)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.DeleteFilesIntentService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.DeleteFilesIntentService


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Ljava/io/FileOutputStream;.write:([B)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
  Discovered taint paths are listed below:
