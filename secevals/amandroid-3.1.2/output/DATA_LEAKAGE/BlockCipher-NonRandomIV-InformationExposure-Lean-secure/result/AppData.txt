Application Name: BlockCipher-NonRandomIV-InformationExposure-Lean-secure.apk
Uses Permissions: 

Component edu.ksu.cs.secure.GradesContentProvider
  Component type: provider
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.secure.AddStudent
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f4b8a)(AddStudent.env,L57)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.secure.AddStudent
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.secure.AddStudent
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f4852)(AddStudent.env,L50)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.secure.MainActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.secure.MainActivity

Component edu.ksu.cs.secure.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f4b8a)(MainActivity.envMain,L97)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.secure.AddStudent
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.secure.AddStudent
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f4852)(MainActivity.envMain,L92)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.secure.MainActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.secure.MainActivity


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Ljava/io/FileOutputStream;.write:([B)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
  Discovered taint paths are listed below:
