Application Name: UnprotectedBroadcastRecv-PrivEscalation-Lean-secure.apk
Uses Permissions: android.permission.SEND_SMS

Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f3f74)(MainActivity.envMain,L21)
      Outgoing Intents via this call:
        Intent:
          Actions:
            edu.ksu.cs.benign.myrecv
          Explicit: false
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MyReceiver

Component edu.ksu.cs.benign.MyReceiver
  Component type: receiver
  Exported: true
  Dynamic Registered: false
  Required Permission: edu.ksu.cs.secure.permission1
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.myrecv"],Categories:["android.intent.category.DEFAULT"])

  Inter-component communication (ICC) Result:



Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/telephony/SmsManager;.sendTextMessage:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V 3>
    <Descriptors: api_sink: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V 1>
  Discovered taint paths are listed below:
