GHERA_HOME=~/ghera
GHERA_APKS=../ghera-apks
OUTPUT=output


execute_ta () {
  outdir=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  start=`date +%s`
  timeout 15m java -jar argus-saf_2.12-3.1.2-assembly.jar t -mo $2 -o $OUTPUT/$2 $1
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/$2/$outdir/time.txt
  if [ -f $OUTPUT/$2/.errorlog ] ; then
    mv $OUTPUT/$2/.errorlog $OUTPUT/$2/$outdir/
  fi
}

execute_am () {
  outdir=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  start=`date +%s`
  timeout 15m java -jar argus-saf_2.12-3.1.2-assembly.jar a -c $2 -o $OUTPUT/$2 $1 | grep -A1 -E "CryptographicMisuse:|SSLTLSMisuse:" > result.txt
  mv result.txt $OUTPUT/$2/$outdir/
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/$2/$outdir/time.txt
  if [ -f $OUTPUT/$2/.errorlog ] ; then
    mv $OUTPUT/$2/.errorlog $OUTPUT/$2/$outdir/
  fi
}

# echo "running Amandroid on Benign apks"
# for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
#   echo "Processing $b"
#   execute_ta $b DATA_LEAKAGE
#   execute_ta $b INTENT_INJECTION
#   execute_ta $b COMMUNICATION_LEAKAGE
#   execute_ta $b PASSWORD_TRACKING
#   execute_ta $b OAUTH_TOKEN_TRACKING
#   execute_am $b CRYPTO_MISUSE
#   execute_am $b SSLTLS_MISUSE
#   echo "done with $b"
# done
echo "running Amandroid on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  execute_ta $s DATA_LEAKAGE
  execute_ta $s INTENT_INJECTION
  execute_ta $s COMMUNICATION_LEAKAGE
  execute_ta $s PASSWORD_TRACKING
  execute_ta $s OAUTH_TOKEN_TRACKING
  execute_am $s CRYPTO_MISUSE
  execute_am $s SSLTLS_MISUSE
  echo "done with $s"
done
echo "running Amandroid on malicious apks"
for m in `ls -1 $GHERA_APKS/malicious/*.apk` ; do
  echo "Processing $m"
  execute_ta $m DATA_LEAKAGE
  execute_ta $m INTENT_INJECTION
  execute_ta $m COMMUNICATION_LEAKAGE
  execute_ta $m PASSWORD_TRACKING
  execute_ta $m OAUTH_TOKEN_TRACKING
  execute_am $m CRYPTO_MISUSE
  execute_am $m SSLTLS_MISUSE
  echo "done with $m"
done
echo "done"
