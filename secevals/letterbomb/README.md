# Tool Setup

The tool is not intended for public use. Please contact the authors for a copy of the tool and the instructions to setup the tool. More information can be found at LetterBomb's [project website](https://seal.ics.uci.edu/projects/letterbomb/). Please follow the instructions in the following sections after setting up the tool

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runLB.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/<benchmark>.txt*

# Reproduce experiment (manually)

XSS vulnerability:

`$ letterbomb/workspace/provisoriam/wv.sh <apk>`

See result in `letterbomb/workspace/provisoriam/logs/edu.uci.seal.cases.analyses.WebViewTransformer-<benchmark>.apk-main.log`

# References

[Letterbomb](http://seal.ics.uci.edu/projects/letterbomb/)
