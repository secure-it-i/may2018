export GHERA_HOME=~/ghera
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/covert/output
export COVERT_HOME=~/rekha-eval/covert/Covert_Engine-2.3/Covert

clean_data () {
  rm -r $COVERT_HOME/ghera_repo/*.apk
  rm -rf $COVERT_HOME/app_repo/ghera_repo/
  echo "clean up done!"
}

execute () {
  cd $COVERT_HOME/
  apkname=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  cp $1 $COVERT_HOME/ghera_repo
  newapkname=${apkname/$2/malicious}
  if [ -f $GHERA_APKS/malicious/$newapkname.apk ] ; then
    cp $GHERA_APKS/malicious/$newapkname.apk $COVERT_HOME/ghera_repo
  fi
  start=`date +%s`
  timeout 15m ./covert.sh ghera_repo/
  end=`date +%s`
  runtime=$((end-start))
  mkdir $OUTPUT/default/$apkname
  echo "$runtime s" > $OUTPUT/default/$apkname/time.txt
  cp -R  $COVERT_HOME/app_repo/ghera_repo/ $OUTPUT/default/$apkname/
}

# echo "running Covert on Benign apks"
# for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
#   echo "Processing $b"
#   execute $b "benign"
#   clean_data
#   echo "done with $b"
# done
echo "running Covert on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  execute $s "secure"
  clean_data
  echo "done with $s"
done
echo "done"
