# Tool Setup

Download the zip file

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runCovert.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/ghera-repo/ghera_repo.xml*

# Reproduce experiment (manually)

Copy the *Benign/Secure* app and *Malicious* app for each benchmark in *ghera_repo* directory

Run Covert on *ghera_repo*

  `$ Covert_Engine-2.3/Covert/covert.sh ghera_repo/`

See the results in `Covert_Engine-2.3/Covert/ghera_repo/`

# References

[Covert](https://seal.ics.uci.edu/projects/covert/index.html)
