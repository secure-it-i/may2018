export ANDROID_HOME=~/Android/Sdk
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/androbugs/output
export ANDROBUGS_HOME=~/rekha-eval/androbugs

clean_data () {
  :
}

execute () {
  mkdir $OUTPUT/default/$2
  start=`date +%s`
  python AndroBugs_Framework/androbugs.py -f $1 -o $OUTPUT/default/$2
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/default/$2/time.txt
}

# echo "running Androbugs on Benign apks"
# for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
#   echo "Processing $b"
#   apkname=`echo $b | grep -o '[^/]*$' | cut -d'.' -f1`
#   execute $b $apkname
#   echo "done with $b"
# done
echo "running Androbugs on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  apkname=`echo $s | grep -o '[^/]*$' | cut -d'.' -f1`
  execute $s $apkname
  echo "done with $s"
done
echo "done"
