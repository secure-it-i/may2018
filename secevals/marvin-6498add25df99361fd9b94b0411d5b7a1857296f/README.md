# Tool Setup

Download *tar.gz* file

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runMarvin.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/result.txt*

# Reproduce experiment (manually)

`cd marvin`

`$ python MarvinStaticAnalyzer.py <apk> > <output>`

# References

[Marvin Static Analyzer](https://github.com/programa-stic/Marvin-static-Analyzer)
