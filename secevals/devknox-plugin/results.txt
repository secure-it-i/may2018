BlockCipher-ECB-InformationExposure-Lean_benign : D
BlockCipher-ECB-InformationExposure-Lean_secure : ND
BlockCipher-NonRandomIV-InformationExposure-Lean_benign : ND
BlockCipher-NonRandomIV-InformationExposure-Lean_secure : ND
ConstantKey-ForgeryAttack-Lean_benign : ND
ConstantKey-ForgeryAttack-Lean_secure : ND
PBE-ConstantSalt-InformationExposure_benign : ND
PBE-ConstantSalt-InformationExposure_secure : ND
DynamicRegBroadcastReceiver-UnrestrictedAccess-Lean_benign : ND
DynamicRegBroadcastReceiver-UnrestrictedAccess-Lean_secure : ND
EmptyPendingIntent-PrivEscalation-Lean_benign : ND
EmptyPendingIntent-PrivEscalation-Lean_secure : ND
HighPriority-ActivityHijack-Lean_benign : ND
HighPriority-ActivityHijack-Lean_secure : ND
ImplicitIntent-ServiceHijack-Lean_benign : ND
ImplicitIntent-ServiceHijack-Lean_secure : ND
ImplicitPendingIntent-IntentHijack-Lean_benign : ND
ImplicitPendingIntent-IntentHijack-Lean_secure : ND
InadequatePathPermission-InformationExposure-Lean_benign : ND
InadequatePathPermission-InformationExposure-Lean_secure : ND
IncorrectHandlingImplicitIntent-UnauthorizedAccess-Lean_benign :  ND
IncorrectHandlingImplicitIntent-UnauthorizedAccess-Lean_secure : ND
NoValidityCheckOnBroadcastMsg-UnintendedInvocation-Lean_benign : ND
NoValidityCheckOnBroadcastMsg-UnintendedInvocation-Lean_secure :  ND
OrderedBroadcast-DataInjection-Lean_benign : ND
OrderedBroadcast-DataInjection-Lean_secure : ND
StickyBroadcast-DataInjection-Lean_benign : ND
StickyBroadcast-DataInjection-Lean_secure : ND
TaskAffinity-ActivityHijack-Lean_benign : ND
TaskAffinity-ActivityHijack-Lean_secure : ND
TaskAffinity-LauncherActivity-PhishingAttack-Lean_benign : ND
TaskAffinity-LauncherActivity-PhishingAttack-Lean_secure : ND
TaskAffinity-PhishingAttack-Lean_benign : ND
TaskAffinity-PhishingAttack-Lean_secure : ND
TaskAffinityAndReparenting-PhishingAndDoSAttack-Lean_benign : ND
TaskAffinityAndReparenting-PhishingAndDoSAttack-Lean_secure : ND
UnprotectedBroadcastRecv-PrivEscalation-Fat_benign : ND
UnprotectedBroadcastRecv-PrivEscalation-Fat_secure : ND
UnprotectedBroadcastRecv-PrivEscalation-Lean_benign : ND
UnprotectedBroadcastRecv-PrivEscalation-Lean_secure : ND
WeakChecksOnDynamicInvocation-DataInjection-Lean_benign : ND
WeakChecksOnDynamicInvocation-DataInjection-Lean_secure : ND
OpenSocket-InformationLeak-Lean_benign : ND
OpenSocket-InformationLeak-Lean_secure : ND
UnEncryptedSocketComm-MITM-Lean_benign : ND
UnEncryptedSocketComm-MITM-Lean_secure : ND
WeakPermission-UnauthorizedAccess-Lean_benign : ND
WeakPermission-UnauthorizedAccess-Lean_secure : ND
ExternalStorage-DataInjection-Lean_benign : ND
ExternalStorage-DataInjection-Lean_secure : ND
ExternalStorage-InformationLeak-Lean_benign : ND
ExternalStorage-InformationLeak-Lean_secure : ND
InternalStorage-DirectoryTraversal-Lean_benign : ND
InternalStorage-DirectoryTraversal-Lean_secure : ND
InternalToExternalStorage-InformationLeak-Lean_benign : ND
InternalToExternalStorage-InformationLeak-Lean_secure : ND
SQLlite-RawQuery-SQLInjection-Lean_benign : ND
SQLlite-RawQuery-SQLInjection-Lean_secure : ND
SQLlite-SQLInjection-Lean_benign : ND
SQLlite-SQLInjection-Lean_secure : ND
CheckCallingOrSelfPermission-PrivilegeEscalation-Lean_benign : D
CheckCallingOrSelfPermission-PrivilegeEscalation-Lean_secure : D
CheckPermission-PrivilegeEscalation-Lean_benign : D
CheckPermission-PrivilegeEscalation-Lean_secure : D
EnforceCallingOrSelfPermission-PrivilegeEscalation-Lean_benign : D
EnforceCallingOrSelfPermission-PrivilegeEscalation-Lean_secure : D
EnforcePermission-PrivilegeEscalation-Lean_benign : D
EnforcePermission-PrivilegeEscalation-Lean_secure : D
HttpConnection-MITM-Lean_benign
HttpConnection-MITM-Lean_secure
IncorrectHostNameVerification-MITM-Lean_benign
IncorrectHostNameVerification-MITM-Lean_secure
InvalidCertificateAuthority-MITM-Lean_benign
InvalidCertificateAuthority-MITM-Lean_secure
UnsafeIntentURLImpl-InformationExposure-Lean_benign
UnsafeIntentURLImpl-InformationExposure-Lean_secure
WebView-NoUserPermission-InformationExposure-Lean_benign
WebView-NoUserPermission-InformationExposure-Lean_secure
WebViewAllowFileAccess-UnauthorizedFileAccess-Lean_benign : ND
WebViewAllowFileAccess-UnauthorizedFileAccess-Lean_secure : ND
WebViewIgnoreSSLWarning-MITM-Lean_benign : ND
WebViewIgnoreSSLWarning-MITM-Lean_secure : ND
WebViewInterceptRequest-MITM-Lean_benign : ND
WebViewInterceptRequest-MITM-Lean_secure : ND
WebViewOverrideUrl-MITM-Lean_benign : ND
WebViewOverrideUrl-MITM-Lean_secure : ND
