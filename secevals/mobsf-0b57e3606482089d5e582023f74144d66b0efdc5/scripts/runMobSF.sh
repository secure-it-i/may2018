#Change these settings to suit your local settings
export ANDROID_HOME=~/Android/Sdk
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/secevals/mobsf-0b57e3606482089d5e582023f74144d66b0efdc5/output
export MOBSF_HOME=~/secevals/mobsf-0b57e3606482089d5e582023f74144d66b0efdc5

# Set apikey
# The API key can be obtained from (http://0.0.0.0:8000/api_docs)
# if MOBSF was started with default settings
apikey=<fill me>

clean_data () {
  md5=`md5sum $1 | cut -d' ' -f1`
  curl -X POST --url http://localhost:8000/api/v1/delete_scan \
    --data "hash=$md5" \
      -H "Authorization:$apikey"
}

upload () {
  md5=`md5sum $1 | cut -d' ' -f1`
  curl -F 'file=@'$1 http://localhost:8000/api/v1/upload -H "Authorization:$apikey"
}

scan () {
  md5=`md5sum $1 | cut -d' ' -f1`
  curl -X POST --url http://localhost:8000/api/v1/scan \
    --data "scan_type=apk&file_name=$2&hash=$md5" \
      -H "Authorization:$apikey"
}

download () {
  if [ ! -d $OUTPUT/default/$2 ] ; then
    mkdir $OUTPUT/default/$2
  fi
  md5=`md5sum $1 | cut -d' ' -f1`
  curl -X POST --url http://localhost:8000/api/v1/download_pdf \
    --data "hash=$md5&scan_type=apk" \
      -H "Authorization:$apikey" > $OUTPUT/default/$2/report.pdf
}

echo "Processing Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  #clean_data $s
  apkname=`echo $s | grep -o '[^/]*$' | cut -d'.' -f1`
  echo "uploading $s"
  upload $s $apkname
  echo "downloading report for $s"
  download $s $apkname
  echo "done with $s"
done
echo "done"
