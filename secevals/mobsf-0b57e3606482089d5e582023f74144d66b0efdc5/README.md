# Tool Setup

Download *tar.gz* file

# install virtualenv

Linux :

`pip3 install virtualenv`

`virtualenv -p python3 venv`

`source venv/bin/activate`

`pip3 install -r Mobile-Security-Framework-MobSF/requirements.txt`


# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

Change the variables in *runMobSF.sh* tto suit your local settings

`$ ./runMobSF.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/report.pdf*

# Reproduce experiment (manually)

`cd Mobile-Security-Framework-MobSF`

`$ python python3 manage.py runserver`

Alternatively, you can also use a pre-built docker image to use MobSF. Follow instructions [here](https://github.com/MobSF/Mobile-Security-Framework-MobSF).

navigate to http://localhost:8000/ to access the MobSF Web interface.

Upload <apk> and see the generated report

# References

[MobSF](https://github.com/MobSF/Mobile-Security-Framework-MobSF)
