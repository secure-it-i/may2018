# Tool Setup

Download *tar.gz* file

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

Clone ghera:

`git clone https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ ghera`

Run script:

`$ cd scripts`

`$ ./runQark.sh -c <config>`

config : *src* or *apk*

# Results

See the evaluation results in *output/<config>/<benchmark>/report/report.html*

# Reproduce experiment (manually)

Analyze apk:

`$ python qark/qarkMain.py --source=1 -p <apk>`

Analyze source:

`python qark/qarkMain.py --source=2 -m ghera/<category>/<benchmark>/<Benign|Secure>/app/src/main/AndroidManifest.xml -a 1`

See the results in *qark/report/*

# References

[Qark](https://github.com/linkedin/qark)
