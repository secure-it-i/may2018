#change these variables to suit your local config
export GHERA_HOME=~/ghera
export GHERA_APKS=~/vulevals/input/ghera-apks
export OUTPUT=~/vulevals/qark-1dd2feaf09612b0af5f3cded6ba945eaaf0a237c/output

execute() {
  outdir=`echo $1 | cut -d'/' -f6`
  echo "running $outdir-$2"
  start=`date +%s`
  python qark/qarkMain.py --source=2 -m $1/AndroidManifest.xml -a 1
  end=`date +%s`
  runtime=$((end-start))
  cp -R qark/report/ $OUTPUT/src/$outdir-$2/
  echo "copied report to $OUTPUT/src/$outdir-$2"
  echo "$runtime s" > $OUTPUT/src/$outdir-$2/time.txt
  echo "done with $outdir-$2"
}

if [ $# -eq 2 ] && [ $1 == "-c" ] ; then
  if [ $2 == "apk" ] ; then
    # echo "running qark on Benign apks"
    # for b in `ls -1 $GHERA_APKS/benign_test/*.apk` ; do
    #   echo "running qark on $b"
    #   outdir=`echo $b | grep -o '[^/]*$'`
    #   start=`date +%s`
    #   python qark/qarkMain.py --source=1 -p $b
    #   end=`date +%s`
    #   runtime=$((end-start))
    #   cp -R qark/report/ $OUTPUT/apk/$outdir
    #   echo "copied report to $OUTPUT/apk/$outdir"
    #   echo "$runtime s" > $OUTPUT/apk/$outdir/time.txt
    #   echo "done with $outdir"
    # done
    echo "running qark on Secure apks"
    for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
      echo "running qark on $s"
      outdir=`echo $s | grep -o '[^/]*$'`
      start=`date +%s`
      python qark/qarkMain.py --source=1 -p $s
      end=`date +%s`
      runtime=$((end-start))
      cp -R qark/report/ $OUTPUT/apk/$outdir/
      echo "copied report to $OUTPUT/apk/$outdir"
      echo "$runtime s" > $OUTPUT/apk/$outdir/time.txt
      echo "done with $outdir"
    done
    echo "done"
  fi
  if [ $2 == "src" ] ; then
    #run qark on Benign app sources
    # echo "running qark on benign benchmarks ... "
    # for srcPath in $GHERA_HOME/ICC/*-Lean/Benign/app/src/main ; do
    #   execute $srcPath "benign"
    # done
    # for srcPath in $GHERA_HOME/Permission/*-Lean/Benign/app/src/main ; do
    #   execute $srcPath "benign"
    # done
    # for srcPath in $GHERA_HOME/Crypto/*-Lean/Benign/app/src/main ; do
    #   execute $srcPath "benign"
    # done
    # for srcPath in $GHERA_HOME/Storage/*-Lean/Benign/app/src/main ; do
    #   execute $srcPath "benign"
    # done
    # for srcPath in $GHERA_HOME/System/*-Lean/Benign/app/src/main ; do
    #   execute $srcPath "benign"
    # done
    # for srcPath in $GHERA_HOME/Networking/*-Lean/Benign/app/src/main ; do
    #   execute $srcPath "benign"
    # done
    # for srcPath in $GHERA_HOME/Web/*-Lean/Benign/app/src/main ; do
    #   execute $srcPath "benign"
    # done
    echo "running qark on secure benchmarks"
    #run qark on Secure app sources
    for srcPath in $GHERA_HOME/ICC/*-Lean/Secure/app/src/main ; do
      execute $srcPath "secure"
    done
    for srcPath in $GHERA_HOME/Permission/*-Lean/Secure/app/src/main ; do
      execute $srcPath "secure"
    done
    for srcPath in $GHERA_HOME/Crypto/*-Lean/Secure/app/src/main ; do
      execute $srcPath "secure"
    done
    for srcPath in $GHERA_HOME/Storage/*-Lean/Secure/app/src/main ; do
      execute $srcPath "secure"
    done
    for srcPath in $GHERA_HOME/System/*-Lean/Secure/app/src/main ; do
      execute $srcPath "secure"
    done
    for srcPath in $GHERA_HOME/Networking/*-Lean/Secure/app/src/main ; do
      execute $srcPath "secure"
    done
    for srcPath in $GHERA_HOME/Web/*-Lean/Secure/app/src/main ; do
      execute $srcPath "secure"
    done
    echo "done"
  fi
else
  echo "required args : -c apk or src"
fi
