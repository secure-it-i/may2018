# Tool Setup

Download *mallodroid.tar.gz*

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runMallodroid.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/log.txt*

# Reproduce experiment (manually)

`mkdir <output> && cd <output>`

`$ mallodroid/mallodroid.py -f <apk> &> <output>`

# References

[Mallodroid](https://github.com/sfahl/mallodroid)
