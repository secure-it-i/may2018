export ANDROID_HOME=~/Android/Sdk
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/mallodroid/output
export MALLODROID_HOME=~/rekha-eval/mallodroid

clean_data () {
  :
}

execute () {
  mkdir $OUTPUT/default/$2
  start=`date +%s`
  $MALLODROID_HOME/mallodroid/mallodroid.py -f $1 &> $OUTPUT/default/$2/log.txt
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/default/$2/time.txt
}

# echo "running Mallodroid on Benign apks"
# for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
#   echo "Processing $b"
#   apkname=`echo $b | grep -o '[^/]*$' | cut -d'.' -f1`
#   execute $b $apkname
#   echo "done with $b"
# done
echo "running Mallodroid on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  apkname=`echo $s | grep -o '[^/]*$' | cut -d'.' -f1`
  execute $s $apkname
  echo "done with $s"
done
echo "done"
