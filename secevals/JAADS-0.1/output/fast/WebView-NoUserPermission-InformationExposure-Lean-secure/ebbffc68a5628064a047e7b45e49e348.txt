{
  "score": 16.0,
  "md5hash": "ebbffc68a5628064a047e7b45e49e348",
  "results": [{
    "desc": "webview addjsinterface code exec",
    "sourceStmt": "virtualinvoke $r4.<android.webkit.WebView: void addJavascriptInterface(java.lang.Object,java.lang.String)>($r5, \"Benign\")",
    "custom": "naive check, may false positive",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onCreate(android.os.Bundle)>"
  }, {
    "desc": "webview addjsinterface code exec",
    "sourceStmt": "virtualinvoke $r4.<android.webkit.WebView: void addJavascriptInterface(java.lang.Object,java.lang.String)>($r5, \"Benign\")",
    "custom": "naive check, may false positive",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onCreate(android.os.Bundle)>"
  }, {
    "desc": "webview addjsinterface code exec",
    "sourceStmt": "virtualinvoke $r4.<android.webkit.WebView: void addJavascriptInterface(java.lang.Object,java.lang.String)>($r5, \"Benign\")",
    "custom": "naive check, may false positive",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onRequestPermissionsResult(int,java.lang.String[],int[])>"
  }, {
    "desc": "Check webview save password disabled or not",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onResume()>"
  }, {
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r3.<android.content.Context: void startActivity(android.content.Intent)>($r2)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.GPSTracker$1: void onClick(android.content.DialogInterface,int)>"
  }]
}