{
  "score": 15.0,
  "md5hash": "a867026c91bf27113bc5961c768414e1",
  "results": [{
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r4.<edu.ksu.cs.benign.MainActivity: void startActivity(android.content.Intent)>($r2)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity$1: void onClick(android.view.View)>"
  }]
}