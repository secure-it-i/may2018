{
  "score": 15.0,
  "md5hash": "68d9e5381f9cb63dea5ae735b4509d80",
  "results": [{
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r4.<edu.cs.ksu.benign.MainActivity: void sendBroadcast(android.content.Intent)>($r3)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.cs.ksu.benign.MainActivity$1: void onClick(android.view.View)>"
  }]
}