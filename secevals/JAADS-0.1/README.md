# Tool Setup

Download the jar file

Download and install Android runtime

  - Download and install [Android Studio](https://developer.android.com/studio/)

Set ANDROID_HOME

  `$ export ANDROID_HOME=<path/to/Android/Sdk>`

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runJAADS.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/<sha>.txt*

# Reproduce experiment (manually)

Perform Full Analysis:

`$  java -jar jade-0.1.jar vulnanalysis -f <apk> -p $ANDROID_HOME/platforms -c config -o <output>`

Perform Fast Analysis:

`$  java -jar jade-0.1.jar vulnanalysis -f <apk> -p $ANDROID_HOME/platforms -c config -o <output> --fastanalysis`

# References

[JAADS](https://github.com/flankerhqd/JAADAS)
