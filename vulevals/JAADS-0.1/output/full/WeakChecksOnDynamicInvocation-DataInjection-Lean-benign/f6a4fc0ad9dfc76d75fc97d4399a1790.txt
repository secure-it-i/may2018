{
  "score": 15.0,
  "md5hash": "f6a4fc0ad9dfc76d75fc97d4399a1790",
  "results": [{
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r2.<edu.ksu.cs.benign.MainActivity: void startActivity(android.content.Intent)>($r3)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity$1: void onClick(android.view.View)>"
  }]
}