{
  "score": 25.0,
  "md5hash": "89a55bfe46cccfcbc684e732c7f82295",
  "results": [{
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r3.<edu.ksu.cs.benign.MainActivity: void sendBroadcast(android.content.Intent)>($r2)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity$1: void onClick(android.view.View)>"
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r0.<edu.ksu.cs.benign.MainActivity: android.content.Intent registerReceiver(android.content.BroadcastReceiver,android.content.IntentFilter)>($r3, $r2)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onCreate(android.os.Bundle)>"
  }]
}