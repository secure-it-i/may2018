{
  "score": 16.599998474121094,
  "md5hash": "3a118160bc151ef0a47359bf7d9af4dc",
  "results": [{
    "desc": "webview addjsinterface code exec",
    "sourceStmt": "virtualinvoke $r4.<android.webkit.WebView: void addJavascriptInterface(java.lang.Object,java.lang.String)>($r5, \"Benign\")",
    "custom": "naive check, may false positive",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onCreate(android.os.Bundle)>"
  }, {
    "desc": "webview addjsinterface code exec",
    "sourceStmt": "virtualinvoke $r4.<android.webkit.WebView: void addJavascriptInterface(java.lang.Object,java.lang.String)>($r5, \"Benign\")",
    "custom": "naive check, may false positive",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onCreate(android.os.Bundle)>"
  }, {
    "desc": "webview addjsinterface code exec",
    "sourceStmt": "virtualinvoke $r4.<android.webkit.WebView: void addJavascriptInterface(java.lang.Object,java.lang.String)>($r5, \"Benign\")",
    "custom": "naive check, may false positive",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onRequestPermissionsResult(int,java.lang.String[],int[])>"
  }, {
    "desc": "Check webview save password disabled or not",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onResume()>"
  }, {
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r3.<android.content.Context: void startActivity(android.content.Intent)>($r2)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.GPSTracker$1: void onClick(android.content.DialogInterface,int)>"
  }, {
    "desc": "webview addjsinterface code exec",
    "sourceStmt": "virtualinvoke $r4.<android.webkit.WebView: void addJavascriptInterface(java.lang.Object,java.lang.String)>($r5, \"Benign\")",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onCreate(android.os.Bundle)>"
  }, {
    "desc": "webview addjsinterface code exec",
    "sourceStmt": "virtualinvoke $r4.<android.webkit.WebView: void addJavascriptInterface(java.lang.Object,java.lang.String)>($r5, \"Benign\")",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onCreate(android.os.Bundle)>"
  }]
}