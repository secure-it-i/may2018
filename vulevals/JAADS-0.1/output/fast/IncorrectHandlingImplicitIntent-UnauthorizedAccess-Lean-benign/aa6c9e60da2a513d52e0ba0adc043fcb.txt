{
  "score": 5.0,
  "md5hash": "aa6c9e60da2a513d52e0ba0adc043fcb",
  "results": [{
    "desc": "NPE_CRASH",
    "sourceStmt": "$z0 = virtualinvoke $r2.<java.lang.String: boolean equals(java.lang.Object)>(\"edu.ksu.cs.benign.SENS_ACTIVITY_ACTION\")",
    "custom": "",
    "vulnKind": 1,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.SensitiveActivity: void onResume()>"
  }, {
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }]
}