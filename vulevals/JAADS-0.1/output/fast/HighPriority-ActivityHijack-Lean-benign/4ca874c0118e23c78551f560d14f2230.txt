{
  "score": 15.0,
  "md5hash": "4ca874c0118e23c78551f560d14f2230",
  "results": [{
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r3.<edu.ksu.cs.benign.HomeActivity: void startActivity(android.content.Intent)>($r2)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.HomeActivity$1: void onClick(android.view.View)>"
  }]
}