export ANDROID_HOME=~/Android/Sdk
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/flowdroid/output
export FLOWDROID_HOME=~/rekha-eval/flowdroid

clean_data () {
  :
}

execute () {
  mkdir $OUTPUT/default/$2
  start=`date +%s`
  java -jar soot-infoflow-cmd-jar-with-dependencies.jar -a $1 -p $ANDROID_HOME/platforms/ -s $FLOWDROID_HOME/SourcesAndSinks.txt -o $OUTPUT/default/$2/result.xml
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/default/$2/time.txt
}

echo "running FlowDroid on Benign apks"
for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
  echo "Processing $b"
  apkname=`echo $b | grep -o '[^/]*$' | cut -d'.' -f1`
  execute $b $apkname
  echo "done with $b"
done
echo "running FlowDroid on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  apkname=`echo $s | grep -o '[^/]*$' | cut -d'.' -f1`
  execute $s $apkname
  echo "done with $s"
done
echo "running FlowDroid on malicious apks"
for m in `ls -1 $GHERA_APKS/malicious/*.apk` ; do
  echo "Processing $m"
  apkname=`echo $m | grep -o '[^/]*$' | cut -d'.' -f1`
  execute $m $apkname
  echo "done with $m"
done
echo "done"
