*************************************************************************
**   AndroBugs Framework - Android App Security Vulnerability Scanner  **
**                            version: 1.0.0                           **
**     author: Yu-Cheng Lin (@AndroBugs, http://www.AndroBugs.com)     **
**               contact: androbugs.framework@gmail.com                **
*************************************************************************
Platform: Android
Package Name: edu.ksu.cs.benign
Package Version Name: 1.0
Package Version Code: 1
Min Sdk: 19
Target Sdk: 25
MD5   : 43bf08c77a5eba252a78b200ee4083a5
SHA1  : a99c602fae4a45d6efca5c758146e185ae767474
SHA256: 76109f8d965caf1eeaaca079e8408bb4e9c1c45f3e65e72b71e684ecfa7c895b
SHA512: 86d7d8eb66397fff0c8f35ac44573c253b3ebd5b8fc9a2b777e8394c6ab32190e956aa5b48d24ca25f8bfa4121a11862454c6c0ad78cef02ff9594a83e4b50fc
Analyze Signature: a07df2fe6c8347d469fbc2478af332ab72cfab43d6cd67d529ff0c9bae66bfccae802b19a5dae40b8d73908a4ddc937804f07afe816176ee7a4e3453ba06ddb9
------------------------------------------------------------------------------------------------
[Critical] <Debug> Android Debug Mode Checking:
           DEBUG mode is ON(android:debuggable="true") in AndroidManifest.xml. This is very dangerous. The attackers will be able to sniffer
           the debug messages by Logcat. Please disable the DEBUG mode if it is a released application.
[Warning] <WebView> WebView Local File Access Attacks Checking:
           Found "setAllowFileAccess(true)" or not set(enabled by default) in WebView. The attackers could inject malicious script into
           WebView and exploit the opportunity to access local resources. This can be mitigated or prevented by disabling local file system
           access. (It is enabled by default)
           Note that this enables or disables file system access only. Assets and resources are still accessible using file:///android_asset
           and file:///android_res.
           The attackers can use "mWebView.loadUrl("file:///data/data/[Your_Package_Name]/[File]");" to access app's local file.
           Reference: (1)https://labs.mwrinfosecurity.com/blog/2012/04/23/adventures-with-android-webviews/
                      (2)http://developer.android.com/reference/android/webkit/WebSettings.html#setAllowFileAccess(boolean)
           Please add or modify "yourWebView.getSettings().setAllowFileAccess(false)" to your WebView:
               Ledu/ksu/cs/benign/MainActivity;->onResume()V
[Warning] <WebView> WebView Potential XSS Attacks Checking:
           Found "setJavaScriptEnabled(true)" in WebView, which could exposed to potential XSS attacks. Please check the web page code
           carefully and sanitize the output:
               => Ledu/ksu/cs/benign/MainActivity;->onResume()V (0x22) ---> Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V
[Info]  AndroidManifest Adb Backup Checking:
           This app has disabled Adb Backup.
[Info] <Command> Runtime Command Checking:
           This app is not using critical function 'Runtime.getRuntime().exec("...")'.
[Info] <Command> Executing "root" or System Privilege Checking:
           Did not find codes checking "root" permission(su) or getting system permission (It's still possible we did not find out).
[Info] <Database> SQLiteDatabase Transaction Deprecated Checking:
           Ignore checking "SQLiteDatabase:beginTransactionNonExclusive" because your set minSdk >= 11.
[Info] <Database> Android SQLite Databases Encryption (SQLite Encryption Extension (SEE)):
           This app is "NOT" using SQLite Encryption Extension (SEE) on Android (http://www.sqlite.org/android) to encrypt or decrpyt
           databases.
[Info] <Database> Android SQLite Databases Encryption (SQLCipher):
           This app is "NOT" using SQLCipher(http://sqlcipher.net/) to encrypt or decrpyt databases.
[Info] <Database><#CVE-2011-3901#> Android SQLite Databases Vulnerability Checking:
           This app is "NOT" using Android SQLite databases.
[Info]  Dynamic Code Loading:
           No dynamic code loading(DexClassLoader) found.
[Info]  External Storage Accessing:
           External storage access not found.
[Info]  File Unsafe Delete Checking:
           Did not detect that you are unsafely deleting files.
[Info] <#BID 64208, CVE-2013-6271#> Fragment Vulnerability Checking:
           Did not detect the vulnerability of "Fragment" dynamically loading into "PreferenceActivity" or "SherlockPreferenceActivity"
[Info] <Framework> Framework - MonoDroid:
           This app is NOT using MonoDroid Framework (http://xamarin.com/android).
[Info] <Hacker> Base64 String Encryption:
           No encoded Base64 String or Urls found.
[Info] <Database><Hacker> Key for Android SQLite Databases Encryption:
           Did not find using the symmetric key(PRAGMA key) to encrypt the SQLite databases (It's still possible that it might use but we
           did not find out).
[Info] <Debug><Hacker> Codes for Checking Android Debug Mode:
           Did not detect codes for checking "ApplicationInfo.FLAG_DEBUGGABLE" in AndroidManifest.xml.
[Info] <Hacker> APK Installing Source Checking:
           Did not detect this app checks for APK installer sources.
[Info] <KeyStore><Hacker> KeyStore File Location:
           Did not find any possible BKS keystores or certificate keystore file (Notice: It does not mean this app does not use keysotre):
[Info] <KeyStore><Hacker> KeyStore Protection Checking:
           Ignore checking KeyStore protected by password or not because you're not using KeyStore.
[Info] <Hacker> Code Setting Preventing Screenshot Capturing:
           Did not detect this app has code setting preventing screenshot capturing.
[Info] <Signature><Hacker> Getting Signature Code Checking:
           Did not detect this app is checking the signature in the code.
[Info]  HttpURLConnection Android Bug Checking:
           Ignore checking "http.keepAlive" because you're not using "HttpURLConnection" and min_Sdk > 8.
[Info] <KeyStore> KeyStore Type Checking:
           KeyStore 'BKS' type check OK
[Info]  Google Cloud Messaging Suggestion:
           Nothing to suggest.
[Info] <#CVE-2013-4787#> Master Key Type I Vulnerability:
           No Master Key Type I Vulnerability in this APK.
[Info]  App Sandbox Permission Checking:
           No security issues "MODE_WORLD_READABLE" or "MODE_WORLD_WRITEABLE" found on 'openOrCreateDatabase' or 'openOrCreateDatabase2' or
           'getDir' or 'getSharedPreferences' or 'openFileOutput'
[Info]  Native Library Loading Checking:
           No native library loaded.
[Info]  AndroidManifest Dangerous ProtectionLevel of Permission Checking:
           No "dangerous" protection level customized permission found (AndroidManifest.xml).
[Info]  AndroidManifest Exported Components Checking:
           No exported components(except for Launcher) for receiving Android or outside applications' actions (AndroidManifest.xml).
[Info]  AndroidManifest PermissionGroup Checking:
           PermissionGroup in permission tag of AndroidManifest sets correctly.
[Info] <Implicit_Intent> Implicit Service Checking:
           No dangerous implicit service.
[Info]  AndroidManifest "intent-filter" Settings Checking:
           "intent-filter" of AndroidManifest.xml check OK.
[Info]  AndroidManifest Normal ProtectionLevel of Permission Checking:
           No default or "normal" protection level customized permission found (AndroidManifest.xml).
[Info] <#CVE-2013-6272#> AndroidManifest Exported Lost Prefix Checking:
           No exported components that forgot to add "android:" prefix.
[Info]  AndroidManifest ContentProvider Exported Checking:
           No exported "ContentProvider" found (AndroidManifest.xml).
[Info] <Sensitive_Information> Getting IMEI and Device ID:
           Did not detect this app is getting the "device id(IMEI)" by "TelephonyManager.getDeviceId()" approach.
[Info] <Sensitive_Information> Getting ANDROID_ID:
           Did not detect this app is getting the 64-bit number "Settings.Secure.ANDROID_ID".
[Info]  Codes for Sending SMS:
           Did not detect this app has code for sending SMS messages (sendDataMessage, sendMultipartTextMessage or sendTextMessage).
[Info] <System> AndroidManifest sharedUserId Checking:
           This app does not use "android.uid.system" sharedUserId.
[Info] <SSL_Security> SSL Implementation Checking (Verifying Host Name in Custom Classes):
           Self-defined HOSTNAME VERIFIER checking OK.
[Info] <SSL_Security> SSL Implementation Checking (Verifying Host Name in Fields):
           Critical vulnerability "ALLOW_ALL_HOSTNAME_VERIFIER" field setting or "AllowAllHostnameVerifier" class instance not found.
[Info] <SSL_Security> SSL Implementation Checking (Insecure component):
           Did not detect SSLSocketFactory by insecure method "getInsecure".
[Info] <SSL_Security> SSL Implementation Checking (HttpHost):
           DEFAULT_SCHEME_NAME for HttpHost check: OK
[Info] <SSL_Security> SSL Connection Checking:
           Did not discover urls that are not under SSL (Notice: if you encrypt the url string, we can not discover that).
[Info] <SSL_Security> SSL Implementation Checking (WebViewClient for WebView):
           Did not detect critical usage of "WebViewClient"(MITM Vulnerability).
[Info] <SSL_Security> SSL Certificate Verification Checking:
           Did not find vulnerable X509Certificate code.
[Info]  Unnecessary Permission Checking:
           Permission 'android.permission.ACCESS_MOCK_LOCATION' sets correctly.
[Info]  Accessing the Internet Checking:
           No HTTP-related connection codes found.
[Info]  AndroidManifest System Use Permission Checking:
           No system-level critical use-permission found.
[Info] <WebView><Remote Code Execution><#CVE-2013-4710#> WebView RCE Vulnerability Checking:
           WebView addJavascriptInterface vulnerabilities not found.
------------------------------------------------------------
AndroBugs analyzing time: 1.648934 secs
Total elapsed time: 8.562309 secs
