# Tool Setup

1. Download the tar file

2. Install SQL Server

3. Set up a user with a username/password and update *build/cc.properties* with the username and password

4. Create a database called *dialdroid*

5. Create the database schema in *dialdroid* by executing the sql script found at [DIALDroid.sql](https://github.com/dialdroid-android/dialdroid-db/blob/master/DIALDroid.sql)

6. Update the *classpath* variable in *build/dialdroid.sh* and set it to *path/to/Android/Sdk/platforms*

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runDIALDROID.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/log.txt*

If the tool failed to process the benchmark then the error will be captured in *output/<config>/<benchmark>/err.txt*

# Reproduce experiment (manually)

Copy *Benign* and *Malicious* of each benchmark in *DIALDroid/ghera_repo*

`$ DIALDroid/build/dialdroid.sh DIALDroid/ghera_repo debug > <output>/log.txt 2> <output>/err.txt`

# References

[DIALDroid](https://github.com/dialdroid-android/DIALDroid)
