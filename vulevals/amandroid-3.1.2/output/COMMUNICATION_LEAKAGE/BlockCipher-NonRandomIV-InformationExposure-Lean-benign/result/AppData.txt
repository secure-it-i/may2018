Application Name: BlockCipher-NonRandomIV-InformationExposure-Lean-benign.apk
Uses Permissions: 

Component edu.ksu.cs.benign.AddStudent
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f48de)(AddStudent.env,L21)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MainActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MainActivity
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f4c52)(AddStudent.env,L28)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.AddStudent
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.AddStudent

Component edu.ksu.cs.benign.GradesContentProvider
  Component type: provider
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f4c52)(MainActivity.envMain,L97)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.AddStudent
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.AddStudent
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f48de)(MainActivity.envMain,L92)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MainActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MainActivity


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Ljava/io/FileOutputStream;.write:([B)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
  Discovered taint paths are listed below:
