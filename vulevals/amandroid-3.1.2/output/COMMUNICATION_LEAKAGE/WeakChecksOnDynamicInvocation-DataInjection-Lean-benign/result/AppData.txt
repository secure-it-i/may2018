Application Name: WeakChecksOnDynamicInvocation-DataInjection-Lean-benign.apk
Uses Permissions: 

Component edu.ksu.cs.benign.FileEditActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f2448)(FileEditActivity.env,L33)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.FileEditActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.FileEditActivity

Component edu.ksu.cs.benign.FIleContentProvider
  Component type: provider
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f2448)(MainActivity.envMain,L112)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.FileEditActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.FileEditActivity


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Ljava/io/FilterOutputStream;.write:([B)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
  Discovered taint paths are listed below:
