Application Name: HttpConnection-MITM-Lean-benign.apk
Uses Permissions: android.permission.INTERNET

Component edu.ksu.cs.benign.ResponseActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.HttpIntentService
  Component type: service
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (HttpIntentService.onHandleIntent,L10d260)(HttpIntentService.env,L32)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.ResponseActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.ResponseActivity

Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (1.onClick,L10d2ea)(MainActivity.envMain,L59)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.HttpIntentService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.HttpIntentService


Taint analysis result:
  Sources found:
    <Descriptors: api_source: Ljava/net/URLConnection;.getInputStream:()Ljava/io/InputStream;>
  Sinks found:
    <Descriptors: api_sink: Ljava/net/URL;.openConnection:()Ljava/net/URLConnection; 0>
  Discovered taint paths are listed below:
