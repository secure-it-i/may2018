Application Name: InternalToExternalStorage-InformationLeak-Lean-benign.apk
Uses Permissions: 

Component edu.ksu.cs.benign.DownloadService
  Component type: service
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.DOWNLOAD"],Categories:["android.intent.category.DEFAULT"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V
      Caller Context: (DownloadService.onHandleIntent,L10d3f0)(DownloadService.envMain,L8)
      Outgoing Intents via this call:
        Intent:
          Explicit: false
          Precise: false

Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (2.onClick,L10d712)(MainActivity.envMain,L35)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.DownloadService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.DownloadService


Taint analysis result:
  Sources found:
    <Descriptors: callback_source: Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V>
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Ljava/io/FileOutputStream;.write:([B)V 1>
    <Descriptors: api_sink: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Ljava/io/FileOutputStream;.write:([B)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
  Discovered taint paths are listed below:
    TaintPath:
      Source: <Descriptors: callback_source: Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V>
      Sink: <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
      Types: maliciousness:information_theft
      The path consists of the following edges ("->"). The nodes have the context information (p1 to pn means which parameter). The source is at the top :
        List(Entry@Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V param: 1, (DownloadService.onHandleIntent,L10d20e)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8) param: 0, Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d26e)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d310)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d3ac)(DownloadService.envMain,L8) param: 0)

    TaintPath:
      Source: <Descriptors: callback_source: Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V>
      Sink: <Descriptors: api_sink: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V 1>
      Types: maliciousness:information_theft
      The path consists of the following edges ("->"). The nodes have the context information (p1 to pn means which parameter). The source is at the top :
        List(Entry@Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V param: 1, (DownloadService.onHandleIntent,L10d20e)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8) param: 0, Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d3f0)(DownloadService.envMain,L8) param: 1)

    TaintPath:
      Source: <Descriptors: callback_source: Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V>
      Sink: <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
      Types: maliciousness:information_theft
      The path consists of the following edges ("->"). The nodes have the context information (p1 to pn means which parameter). The source is at the top :
        List(Entry@Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V param: 1, (DownloadService.onHandleIntent,L10d20e)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8) param: 0, Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d328)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d394)(DownloadService.envMain,L8) param: 0, Call@(DownloadService.onHandleIntent,L10d394)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d39c)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d3ac)(DownloadService.envMain,L8) param: 1)

    TaintPath:
      Source: <Descriptors: callback_source: Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V>
      Sink: <Descriptors: api_sink: Ljava/io/FileOutputStream;.write:([B)V 1>
      Types: maliciousness:information_theft
      The path consists of the following edges ("->"). The nodes have the context information (p1 to pn means which parameter). The source is at the top :
        List(Entry@Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V param: 1, (DownloadService.onHandleIntent,L10d20e)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8) param: 0, Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d26e)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d340)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d348)(DownloadService.envMain,L8) param: 1)

    TaintPath:
      Source: <Descriptors: callback_source: Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V>
      Sink: <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
      Types: maliciousness:information_theft
      The path consists of the following edges ("->"). The nodes have the context information (p1 to pn means which parameter). The source is at the top :
        List(Entry@Ledu/ksu/cs/benign/DownloadService;.onHandleIntent:(Landroid/content/Intent;)V param: 1, (DownloadService.onHandleIntent,L10d20e)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8) param: 0, Call@(DownloadService.onHandleIntent,L10d212)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d26e)(DownloadService.envMain,L8), Call@(DownloadService.onHandleIntent,L10d310)(DownloadService.envMain,L8) param: 1)