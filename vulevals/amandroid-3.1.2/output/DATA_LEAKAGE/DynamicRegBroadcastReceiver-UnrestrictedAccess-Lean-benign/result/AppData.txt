Application Name: DynamicRegBroadcastReceiver-UnrestrictedAccess-Lean-benign.apk
Uses Permissions: 

Component edu.ksu.cs.benign.EmailActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L10a3c4)(MainActivity.envMain,L41)
      Outgoing Intents via this call:
        Intent:
          Actions:
            edu.ksu.cs.action.EMAIL
          Explicit: false
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.EmailBroadcastRecv

Component edu.ksu.cs.benign.EmailBroadcastRecv
  Component type: receiver
  Exported: true
  Dynamic Registered: true
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.action.EMAIL"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (EmailBroadcastRecv.onReceive,L10a356)(EmailBroadcastRecv.env,L60)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.EmailActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.EmailActivity


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/content/ContextWrapper;.sendBroadcast:(Landroid/content/Intent;)V 1>
  Discovered taint paths are listed below:
