Application Name: OrderedBroadcast-DataInjection-Lean-benign.apk
Uses Permissions: android.permission.PROCESS_OUTGOING_CALLS, android.permission.CALL_PHONE

Component edu.ksu.cs.benign.FormatOutgoingCallReceiver
  Component type: receiver
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.NEW_OUTGOING_CALL"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L0f14b8)(MainActivity.envMain,L30)
      Outgoing Intents via this call:
        Intent:
          Actions:
            android.intent.action.CALL
          Data:
            schemes= tel host= null port= null path= null pathPrefix= null pathPattern= null
          Explicit: false
          Precise: true


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
  Discovered taint paths are listed below:
