Application Name: TaskAffinity-PhishingAttack-Lean-benign.apk
Uses Permissions: android.permission.GET_TASKS

Component edu.ksu.cs.benign.LoginActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L10ac8e)(LoginActivity.envMain,L26)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.HomeActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.HomeActivity
    ICC call details are listed below:
      Caller Procedure: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V
      Caller Context: (2.onClick,L10abfc)(LoginActivity.envMain,L21)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.ImageEditor
          Actions:
            Home
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.ImageEditor
    ICC call details are listed below:
      Caller Procedure: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V
      Caller Context: (1.onClick,L10ab70)(LoginActivity.envMain,L31)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.CameraActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.CameraActivity

Component edu.ksu.cs.benign.HomeActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V
      Caller Context: (1.onClick,L10ab70)(HomeActivity.env,L77)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.CameraActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.CameraActivity
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L10ac8e)(HomeActivity.env,L72)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.HomeActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.HomeActivity
    ICC call details are listed below:
      Caller Procedure: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V
      Caller Context: (2.onClick,L10abfc)(HomeActivity.env,L62)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.ImageEditor
          Actions:
            Home
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.ImageEditor

Component edu.ksu.cs.benign.CameraActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.ImageEditor
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:



Taint analysis result:
  Sources found:
    <Descriptors: api_source: Landroid/app/Activity;.findViewById:(I)Landroid/view/View;>
  Sinks found:
    <Descriptors: api_sink: Landroid/app/Activity;.setResult:(ILandroid/content/Intent;)V 2>
  Discovered taint paths are listed below:
