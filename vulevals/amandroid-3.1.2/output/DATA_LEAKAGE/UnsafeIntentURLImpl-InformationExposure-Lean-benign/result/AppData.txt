Application Name: UnsafeIntentURLImpl-InformationExposure-Lean-benign.apk
Uses Permissions: android.permission.INTERNET, android.permission.ACCESS_FINE_LOCATION, android.permission.ACCESS_COARSE_LOCATION

Component edu.ksu.cs.benign.DisplayActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.intent.action.DISP"],Categories:["android.intent.category.BROWSABLE","android.intent.category.DEFAULT"],Data:[(Schemes:<"benign">,Hosts:<"ghera">,Ports:<"">,Paths:<"/">,)])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/Context;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (MyWebViewClient.getUrlOverrideSettings,L0f4158)(MyWebViewClient.shouldOverrideUrlLoading,L0f41d0)
      Outgoing Intents via this call:
        Intent:
          Data:
            schemes= null host= null port= null path= null pathPrefix= null pathPattern= null
          Explicit: false
          Precise: false
          ICC Targets:
            edu.ksu.cs.benign.MainActivity
            edu.ksu.cs.benign.DisplayActivity
    ICC call details are listed below:
      Caller Procedure: Landroid/content/Context;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (MyWebViewClient.getUrlOverrideSettings,L0f4158)(MyWebViewClient.shouldOverrideUrlLoading,L0f41b4)
      Outgoing Intents via this call:
        Intent:
          Explicit: false
          Precise: false
          ICC Targets:
            edu.ksu.cs.benign.MainActivity
            edu.ksu.cs.benign.DisplayActivity


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/content/Context;.startActivity:(Landroid/content/Intent;)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/content/Context;.startActivity:(Landroid/content/Intent;)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
  Discovered taint paths are listed below:
