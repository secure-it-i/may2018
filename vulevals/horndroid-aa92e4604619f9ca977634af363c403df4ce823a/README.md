# Tool Setup

Download the tar file

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runHD.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/app.log*

# Reproduce experiment (manually)

`cd horndroid/target`

`$ java -jar fshorndroid-0.0.1.jar / ./ <apk>`

See the results in *logs/app.log*

# References

[HornDroid](https://github.com/ylya/horndroid)
