export ANDROID_HOME=~/Android/Sdk
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/maldrolyzer/output
export MALDROLYZER_HOME=~/rekha-eval/maldrolyzer

clean_data () {
  :
}

execute () {
  mkdir $OUTPUT/default/$2
  start=`date +%s`
  python $MALDROLYZER_HOME/maldrolyzer/maldrolyzer.py $1 &> $OUTPUT/default/$2/log.txt
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/default/$2/time.txt
}

echo "running Androwarn on Malicious apks"
for m in `ls -1 $GHERA_APKS/malicious/*.apk` ; do
  echo "Processing $m"
  apkname=`echo $m | grep -o '[^/]*$' | cut -d'.' -f1`
  execute $m $apkname
  echo "done with $m"
done
