# Rekha: Evaluating Effectiveness of Free Android App Security Analysis Tools in Detecting Known Vulnerabilities

This repository contains results of evaluating freely available tools related to Android security. The evaluation considered 64 tools but evaluated only  tools that could be used off-the-shelf to detect known vulnerabilities (malicious behavior) with no or minimal configuration.  Consequently, 19 tools were evaluated.  *Considered Tools* section lists the tool that were considered and their details.

The 19 tools selected for evaluation were evaluated against [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) -- An open source repository of Android app vulnerability benchmarks.  Each benchmark has a *Benign* app that captures a specific vulnerability, a *Malicious* app that exploits *Benign*, and a *Secure* app that does not exhibit the vulnerability captured by *Benign*.  The 14 vulnerability detection tools out of the 19 selected tools were executed against *Benign* and *Secure* apps of the benchmarks. The reports and logs from these executions can be found in the *vulevals* and *secevals* folders, respectively. The remaining 5 malicious behavior detection tools were executed against *Malicious* apps of the benchmarks. The reports and logs for these tools can be found in the *malevals* folder.

We performed an experiment to determine the difference between positive benchmarks i.e., *Benign* apps in Ghera and negative benchmarks i.e., *Secure* apps in Ghera in terms of API usage. We also determined the differences in API usage between benchmarks detected by some tool and benchmarks detected by no tool. Further, we calculated the evaluation measures like Markedness and Informedness of each of the 14 vulnerability detection tools. The results can be found in the *output* folder. All the files in the *output* folder can be re-produced by running *scripts/masterRun.sh*. 

The manuscript is available [here](https://arxiv.org/abs/1806.09059).

## Considered Tools

The following table lists the tools were considered for the evaluation. Each tool is accompanied with a link to the repository, a one line description of the tool, and a brief reason why it was rejected for this evaluation (if it was rejected).

| Tool        | Description           | Reason for rejection  |
| ------------- |:-------------|:-----|
| [Amandroid](http://pag.arguslab.org/argus-saf) | A static analysis framework with built-in checkers to look for vulnerabilities in Android apps | |
| [AndroBugs](https://github.com/AndroBugs/AndroBugs_Framework) | A vulnerability scanner for Android apps | |
| [AndroTotal](https://andrototal.org/) | A free service to scan malicious apps | |
| [AndroWarn](https://github.com/maaaaz/androwarn/) | A tool that detects malicious Android apps via static analysis | Not actively maintained; Fails to process apks that target API levels below 19 |
| [Android Hooker](https://github.com/AndroidHooker/hooker) | An opensource project to help  dynamic analyses of Android applications | Enables analysis; Not a detection tool
| [Android Malware Analysis Toolkit](http://www.mobilemalware.com.br/amat/download.html) | A Linux distro focused on Mobile Malware Analysis for Android | Enables analysis; Not a detection tool
| [Android Tamer](https://androidtamer.com/) | A virtual environment to enable security analysis | Enables analysis; Not a detection tool
| [Androl4b](https://github.com/sh4hin/Androl4b) | A Virtual Machine For Assessing Android applications, Reverse Engineering and Malware Analysis | Enables analysis; Not a detection tool
| [ApkAnalyser](https://github.com/sonyxperiadev/ApkAnalyser)      | A static, virtual analysis tool for examining the source code of Android apps      |  Enables analysis; Not a detection tool |
| [ApkCombiner](https://github.com/lilicoding/ApkCombiner) | A tool to combine multiple apks into one apk | Enables analysis; Not a detection tool
| [ApkInspector](https://github.com/honeynet/apkinspector/) | A GUI tool for analysts to analyze the Android applications      |  Enables analysis; Not a detection tool |
| [AppAudit](http://appaudit.io/#)      | Detects data leaks | The *scan* feature does not work |
| [AppCritique](https://appcritique.boozallen.com/upload) | An online tool that detects several vulnerabilities in Android apps | |
| [AppGuard](http://www.srt-appguard.com/en/) | A tool to manage app permissions | Tool documentation in German
| [AppRay](http://app-ray.co/) | A tool to identify vulenrabilities automatically | Paid tool
| [Aquifer](https://wspr.csc.ncsu.edu/aquifer/) | An OS framework to prevent accidental data disclosure | Enforces security; Not a detection tool
| [ASM](http://www.androidsecuritymodules.org/) | A programmable interface for defining new reference monitors for Android | Enforces security; Not a detection tool |
| [Aurasium](https://github.com/xurubin/aurasium) | An automatic apk repackaging and security policy enforcement tool | Enforces security; Not a detection tool; Not applicable Android API 19 and above
| [AutoCog](https://github.com/prashant31191/AutoCog) |  tool to assess how well an Android application description reveal the security-concerned permission in semantics level | Enables analysis; Not a detection tool
| [BlueSeal](http://blueseal.cse.buffalo.edu/multiflow.html) | A tool to detect malware using multi-flows and API patterns | Source code unavailable at the time of evaluation |
| [CFGScanDroid](https://github.com/douggard/CFGScanDroid) | A utility for comparing CFG signatures to CFGs of Android methods | Enables analysis; Not a detection tool
| [ConDroid](https://github.com/JulianSchuette/ConDroid) | Performs concolic execution to generate test inputs | Enables analysis; Not a detection tool
| [CopperDroid](http://copperdroid.isg.rhul.ac.uk/copperdroid/) | An online tool that performs automatic dynamic analysis to detect malicious behavior | The APK upload service does not work
| [Covert](https://www.ics.uci.edu/~seal/projects/covert/) | A tool for compositional verification of Android inter-application vulnerabilities | |
| [CuckooDroid](https://github.com/idanr1986/cuckoo-droid) | A tool to perform automated Malware analysis of Android apps | Fails to complete analysis; forcefully timed out after 30 mins |
| [DECAF](https://github.com/sycurelab/DECAF) | A binary analysis platform | Enables analysis; Not a detection tool
| [DIALDroid](https://github.com/dialdroid-android/DIALDroid) | A tool to detect inter-app vulnerabilities | |
| [DIDFAIL](https://www.cs.cmu.edu/~wklieber/didfail/) | A static taint analyzer for Android apps | Fails to build based on the instruction on their webpage |
| [DRACO](https://github.com/rishabhgpt/draco) | A tool to classify apps as Malicious and Benign | No documentation about usage
| [DeepDroid](https://github.com/fitzlee/DeepDroid) | No information provided about the tool except source code | No documentation about usage
| [DevKnox](https://devknox.io/) | An Android Studio Plugin that detects vulnerabilities in Android apps | |
| [DroidLegacy](https://bitbucket.org/srl/droidlegacy/src/master/) | No information provided about the tool except source code | No documentation about usage
| [DroidSafe](https://github.com/MIT-PAC/droidsafe-src) | A static information flow analysis system to detect and remove malicious code from Android apps | Analysis fails to terminate; forcefully timed out after running on a 64 GB ubuntu machine for 15 mins |
| [DroidSearch/DroidSIFT](https://sites.google.com/site/hengyinweb/announcements/publicwebservicefordroidsiftisup) | An online classification service for Android malware | Unavailability of online service
| [DroidSec/HickWall](http://analysis.droid-sec.com/) | A tool that detects malicious behavior based on deep learning techniques | |
| [Drozer](https://labs.mwrinfosecurity.com/tools/drozer/) | A Security Assessment Framework to analyze Android apps | Enables analysis; Not a detection tool
| [FLowDroid](https://blogs.uni-paderborn.de/sse/tools/flowdroid/) | A static taint analysis tool for Android apps | |
| [FixDroid](https://plugins.jetbrains.com/plugin/9497-fixdroid) | An Android Studio plugin that provides warning to developers about potential vulnerabilities | |
| [FlaskDroid](http://www.flaskdroid.org/index.html) | A generic security architecture for the Android OS that can serve as a flexible and effective ecosystem to instantiate different security solutions | A generic security architecture for the Android OS; Not a detection tool
| [Gran'n Run](https://github.com/lukeFalsina/Grab-n-Run) | A Java Library to help implement secure dynamic code loading in Android apps | Enables secure development; Not a detection tool
| [HornDroid](https://github.com/ylya/horndroid) | A static analysis tool that detects sensitive information leak in Android apps | |
| [IccTA](https://github.com/lilicoding/soot-infoflow-android-iccta) | A tool for inter-component Taint analysis in Android | Fails to analyze apps due to logical failures |
| [JAADS](https://github.com/flankerhqd/JAADAS) | A static analysis that detects vulnerabilities in Android apps | |
| [LetterBomb](https://seal.ics.uci.edu/projects/letterbomb/) | A tool to automatically generate exploit for Android apps | Not applicable to [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) benchmarks |
| [MARA](https://github.com/xtiankisutsa/MARA_Framework) | A toolkit that puts together commonly used mobile application reverse engineering and analysis tools to assist in testing mobile applications | Enables analysis; Not a detection tool |
| [Maldrolyzer](https://github.com/maldroid/maldrolyzer) | A framework to extract "actionable" data from Android malware (C&Cs, phone numbers etc.) | |
| [Mallodroid](https://github.com/sfahl/mallodroid) | A tool to detect broken SSL certificate valiadation in Android apps | |
| [Marvin-SF](https://github.com/programa-stic/Marvin-static-Analyzer) | An Android app vulnerability scanner | |
| [MobSF](https://github.com/MobSF/Mobile-Security-Framework-MobSF) | An automated pen-testing framework capable of performing static analysis and dynamic analysis to uncover vulnerabilities in Android apps | |
| [NVISO APK Scan](https://apkscan.nviso.be/) | An online service that scans malicious apps | |
| [PScout](http://pscout.csl.toronto.edu/) | A tool to analyse permission usage in Android apps | Not applicable to [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) benchmarks
| [ProbeDroid](https://github.com/ZSShen/ProbeDroid) | A dynamic Java code instrumentation kit for Android apps | Enables analysis; Not a detection tool
| [Qark](https://github.com/linkedin/qark/) | A tool to look for several security related Android application vulnerabilities | |
| [SAAF](https://github.com/SAAF-Developers/saaf) | A static analysis framework for Android apps | Enables analysis; Not a detection tool
| [SMV_Hunter](https://github.com/utds3lab/SMVHunter) | A tool-set for performing large-scale automated detection of SSL/TLS man-in-the-middle vulnerabilities in Android apps | Targets API 18 and below while Ghera targets API 19 and higher |
| [SPARTA](https://www.cs.washington.edu/sparta) | A tool to verify that apps are not malware | Cannot be used off-the-shelf; Apps need to be annotated with security types |
| [ScanDroid](https://www.cs.umd.edu/~avik/papers/scandroidascaa.pdf) | A tool that scans Android apps for inconcsistent data flows | Not maintained actively
| [SmaliSCA](https://github.com/dorneanu/smalisca) | Static Code Analysis for Smali files | Enables analysis; Not a detection tool
| [StaDyna](https://github.com/zyrikby/StaDynA) | A tool to address the problem dynamic code updates in Android apps | Not applicable to [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) benchmarks
| [TaintDroid](http://www.appanalysis.org/index.html) | A realtime monitoring tool that analyses how private information is obtained and released by Android apps | Not applicable Android API 19 and above
| [TraceDroid](http://tracedroid.few.vu.nl/) | A tool records the behavior of an Android app via automaed dynamic analysis | Fails to process apks that target API levels below 19 |
| [TreeDroid](http://prosper.sics.se/treedroid.html) | A security policy specification and enforcement framework for Android | Enforces security; Not a detection tool
| [VirusTotalTotal](https://www.virustotal.com/#/home/upload) | An online service that scans malicious apps | |
| [WeChecker](https://github.com/TRUEJASONFANS/Wechecker) | A tool to check for Privilege Escalation | Not maintained actively; No documentation for using the tool
