# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause "New" and "Revised" License
#
# Author: Venkatesh-Prasad Ranganath


data <- read.csv("input/data/verdict-confusion-matrix.csv", header=T)
rownames(data) <- data$Tool
data$Tool <- NULL
data$TP <- rowSums(data[,c(seq(1, 28, 4))])
data$FP <- rowSums(data[,c(seq(2, 28, 4))])
data$FN <- rowSums(data[,c(seq(3, 28, 4))])
data$TN <- rowSums(data[,c(seq(4, 28, 4))])
result <- t(as.data.frame(apply(data, 1, function (r) {
  tmp1 <- unname(unlist(r))
  tmp3 <- sapply(seq(1, 32, 4), function(i) {
    tmp2 <- tmp1[i:(i+3)]
    rp <- ifelse(sum(tmp2[c(1,3)]) > 0, sum(tmp2[c(1,3)]), 0)
    rn <- ifelse(sum(tmp2[c(2,4)]) > 0, sum(tmp2[c(2,4)]), 0)
    pp <- ifelse(sum(tmp2[c(1,2)]) > 0, sum(tmp2[c(1,2)]), 0)
    pn <- ifelse(sum(tmp2[c(3,4)]) > 0, sum(tmp2[c(3,4)]), 0)
    tpr <- tmp2[1]/rp
    tnr <- tmp2[4]/rn
    informedness <- tpr + tnr - 1
    tpa <- tmp2[1]/pp
    tna <- tmp2[4]/pn
    markedness <- tpa + tna - 1
    prec <- tmp2[1]/pp
    recall <- tmp2[1]/rp
    f <- 2 * prec * recall / ifelse(prec + recall > 0, prec + recall, 1)
    c(f, prec, recall, informedness, markedness)
  })
})))

colnames(result) <- sapply(c("Crypto", "ICC", "Net", "Perm", "Store", "Sys", "Web", "All"), function(i) {
  c(paste(i, "F-Score"), paste(i, "Precision"), paste(i, "Recall"), 
    paste(i, "Informedness"), paste(i, "Markedness"))
})

result[result=="NaN"] <- NA
write.csv(format(result, digits=2), "output/evaluation-measures.csv", quote=F)
