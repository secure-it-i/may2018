/*
 * Copyright (c) 2018, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Joydeep Mitra
 */

def cli = new CliBuilder(usage:"groovy prepare.groovy")
cli.f1(longOpt:'file1', args:1, argName:'file1', required:true,
        'Name of mapping file')
cli.sf(longOpt:'inpFile1', args:1, argName:'inpFile1', required:true,
        'Name of file containing security related apis')
cli.if(longOpt:'inpFile2', args:1, argName:'inpFile2', required:true,
        'Name of apis to ignore file')
cli.o1(longOpt:'outFile1', args:1, argName:'outFile1', required:true,
        'Name of output file containing relevant apis')
cli.o2(longOpt:'outFile2', args:1, argName:'outFile2', required:true,
        'Name of output file containing seucrity apis')

def options = cli.parse(args)
if (!options) {
    return
}

def file1 = new File(options.f1).readLines()
println "loaded " + options.f1

def rel = new File(options.if).readLines()
println "loaded ignore file"
def sec = new File(options.sf).collect{it.split("!")[1]}
println "loaded sec file"

new File(options.o1).withPrintWriter{ def writer ->
  file1.findAll {
    c = it.split("!")[1]
    (!(rel.contains(c)))
  }.sort().each{writer.println(it.split("!")[1])}
}

new File(options.o2).withPrintWriter{ def writer ->
  file1.findAll {
    c = it.split("!")[1]
    ((sec.contains(c)))
  }.sort().each{writer.println(it.split("!")[1])}
}
