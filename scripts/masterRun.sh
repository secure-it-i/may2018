#!/usr/bin/env bash

# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause License
#
# Author: Joydeep Mitra

INPUT=input/data

RN_APIS=$INPUT/ghera-secure-id-api-mapping.csv
RP_APIS=$INPUT/ghera-benign-id-api-mapping.csv
FN_APIS=$INPUT/fn-ghera-benign-id-api-mapping.csv
TP_APIS=$INPUT/tp-ghera-benign-id-api-mapping.csv
APIS_TO_IGNORE=$INPUT/apis-to-ignore.txt
SECURITY_APIS_BENIGN=$INPUT/security/ghera-benign-id-api-mapping.csv
SECURITY_APIS_SECURE=$INPUT/security/ghera-secure-id-api-mapping.csv

OUTPUT=output

OUT_RP_APIS=$OUTPUT/rp-api-list.txt
OUT_RP_SEC_APIS=$OUTPUT/rp-sec-api-list.txt
OUT_RN_APIS=$OUTPUT/rn-api-list.txt
OUT_RN_SEC_APIS=$OUTPUT/rn-sec-api-list.txt
OUT_TP_APIS=$OUTPUT/tp-api-list.txt
OUT_TP_SEC_APIS=$OUTPUT/tp-sec-api-list.txt
OUT_FN_APIS=$OUTPUT/fn-api-list.txt
OUT_FN_SEC_APIS=$OUTPUT/fn-sec-api-list.txt
OUT_ONLY_RP_APIS_RN=$OUTPUT/only-rp-apis-rn.txt
OUT_ONLY_RP_SEC_APIS_RN=$OUTPUT/only-rp-sec-apis-rn.txt
OUT_ONLY_RN_APIS_RP=$OUTPUT/only-rn-apis-rp.txt
OUT_ONLY_RN_SEC_APIS_RP=$OUTPUT/only-rn-sec-apis-rp.txt
OUT_COMMON_RP_RN_APIS=$OUTPUT/common-rp-rn-apis.txt
OUT_COMMON_RP_RN_SEC_APIS=$OUTPUT/common-rp-rn-sec-apis.txt
OUT_ONLY_FN_APIS_RN=$OUTPUT/only-fn-apis-rn.txt
OUT_ONLY_FN_SEC_APIS_RN=$OUTPUT/only-fn-sec-apis-rn.txt
OUT_ONLY_RN_APIS_FN=$OUTPUT/only-rn-apis-fn.txt
OUT_ONLY_RN_SEC_APIS_FN=$OUTPUT/only-rn-sec-apis-fn.txt
OUT_COMMON_FN_RN_APIS=$OUTPUT/common-fn-rn-apis.txt
OUT_COMMON_FN_RN_SEC_APIS=$OUTPUT/common-fn-rn-sec-apis.txt
OUT_ONLY_TP_APIS_RN=$OUTPUT/only-tp-apis-rn.txt
OUT_ONLY_TP_SEC_APIS_RN=$OUTPUT/only-tp-sec-apis-rn.txt
OUT_ONLY_RN_APIS_TP=$OUTPUT/only-rn-apis-tp.txt
OUT_ONLY_RN_SEC_APIS_TP=$OUTPUT/only-rn-sec-apis-tp.txt
OUT_COMMON_TP_RN_APIS=$OUTPUT/common-tp-rn-apis.txt
OUT_COMMON_TP_RN_SEC_APIS=$OUTPUT/common-tp-rn-sec-apis.txt
OUT_ONLY_RN_APIS_TP=$OUTPUT/only-rn-apis-tp.txt
OUT_ONLY_RN_SEC_APIS_TP=$OUTPUT/only-rn-sec-apis-tp.txt
OUT_ONLY_TP_APIS_FN=$OUTPUT/only-tp-apis-fn.txt
OUT_ONLY_TP_SEC_APIS_FN=$OUTPUT/only-tp-sec-apis-fn.txt
OUT_ONLY_FN_APIS_TP=$OUTPUT/only-fn-apis-tp.txt
OUT_ONLY_FN_SEC_APIS_TP=$OUTPUT/only-fn-sec-apis-tp.txt
OUT_COMMON_TP_FN_APIS=$OUTPUT/common-tp-fn-apis.txt
OUT_COMMON_TP_FN_SEC_APIS=$OUTPUT/common-tp-fn-sec-apis.txt
OUT_ONLY_FN_RN_APIS=$OUTPUT/only-fn-rn-apis.txt
OUT_ONLY_TP_RN_APIS=$OUTPUT/only-tp-rn-apis.txt
OUT_COMMON_TP_FN_RN_APIS=$OUTPUT/common-tp-fn-rn-apis.txt

if [ ! -d $OUTPUT ] ; then
  echo "output folder absent. created one!"
  mkdir $OUTPUT
fi

######################################################################3
#prepare the api files for rp,rn,fn, and tp
######################################################################3

echo "preparing api files for rp"
groovy scripts/prepare.groovy -f1 $RP_APIS -sf $SECURITY_APIS_BENIGN -if $APIS_TO_IGNORE -o1 $OUT_RP_APIS -o2 $OUT_RP_SEC_APIS
echo "preparing api files for rn"
groovy scripts/prepare.groovy -f1 $RN_APIS -sf $SECURITY_APIS_SECURE -if $APIS_TO_IGNORE -o1 $OUT_RN_APIS -o2 $OUT_RN_SEC_APIS
echo "preparing api files for fn"
groovy scripts/prepare.groovy -f1 $FN_APIS -sf $SECURITY_APIS_BENIGN -if $APIS_TO_IGNORE -o1 $OUT_FN_APIS -o2 $OUT_FN_SEC_APIS
echo "preparing api files for tp"
groovy scripts/prepare.groovy -f1 $TP_APIS -sf $SECURITY_APIS_BENIGN -if $APIS_TO_IGNORE -o1 $OUT_TP_APIS -o2 $OUT_TP_SEC_APIS

######################################################################3
#Generate Analysis files
######################################################################3
echo "Generating analysis files"
groovy scripts/diff.groovy -f1 $OUT_RP_APIS -f2 $OUT_RN_APIS -o1 $OUT_ONLY_RP_APIS_RN -o2 $OUT_ONLY_RN_APIS_RP -o3 $OUT_COMMON_RP_RN_APIS
groovy scripts/diff.groovy -f1 $OUT_RP_SEC_APIS -f2 $OUT_RN_SEC_APIS -o1 $OUT_ONLY_RP_SEC_APIS_RN -o2 $OUT_ONLY_RN_SEC_APIS_RP -o3 $OUT_COMMON_RP_RN_SEC_APIS
groovy scripts/diff.groovy -f1 $OUT_RN_APIS -f2 $OUT_FN_APIS -o1 $OUT_ONLY_FN_APIS_RN -o2 $OUT_ONLY_RN_APIS_FN -o3 $OUT_COMMON_FN_RN_APIS
groovy scripts/diff.groovy -f1 $OUT_RN_SEC_APIS -f2 $OUT_FN_SEC_APIS -o1 $OUT_ONLY_FN_SEC_APIS_RN -o2 $OUT_ONLY_RN_SEC_APIS_FN -o3 $OUT_COMMON_FN_RN_SEC_APIS
groovy scripts/diff.groovy -f1 $OUT_TP_APIS -f2 $OUT_RN_APIS -o1 $OUT_ONLY_TP_APIS_RN -o2 $OUT_ONLY_RN_APIS_TP -o3 $OUT_COMMON_TP_RN_APIS
groovy scripts/diff.groovy -f1 $OUT_TP_SEC_APIS -f2 $OUT_RN_SEC_APIS -o1 $OUT_ONLY_TP_SEC_APIS_RN -o2 $OUT_ONLY_RN_SEC_APIS_TP -o3 $OUT_COMMON_TP_RN_SEC_APIS
groovy scripts/diff.groovy -f1 $OUT_TP_APIS -f2 $OUT_FN_APIS -o1 $OUT_ONLY_TP_APIS_FN -o2 $OUT_ONLY_FN_APIS_TP -o3 $OUT_COMMON_TP_FN_APIS
groovy scripts/diff.groovy -f1 $OUT_TP_SEC_APIS -f2 $OUT_FN_SEC_APIS -o1 $OUT_ONLY_TP_SEC_APIS_FN -o2 $OUT_ONLY_FN_SEC_APIS_TP -o3 $OUT_COMMON_TP_FN_SEC_APIS
groovy scripts/diff.groovy -f1 $OUT_COMMON_FN_RN_APIS -f2 $OUT_COMMON_TP_RN_APIS -o1 $OUT_ONLY_FN_RN_APIS -o2 $OUT_ONLY_TP_RN_APIS -o3 $OUT_COMMON_TP_FN_RN_APIS

######################################################################3
#Collect API count
######################################################################3

echo "collect api count"
groovy scripts/make-api-usage-matrix.groovy

######################################################################3
#Measure Evaluation measure
######################################################################3
echo "Calculating evalution measures"
Rscript ./scripts/calculate-eval-measures.r
