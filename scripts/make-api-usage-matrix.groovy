def outFolder = "output"
def out = new File(outFolder + "/api-usage-matrix.csv")
def head = ",RP vs RN,RP vs RN (security),FN vs RN,FN vs RN (security),TP vs RN,TP vs RN (security),TP vs FN,TP vs FN (security),FN-RN vs TP-RN\n"
out.write(head)
def comm_rp_vs_rn = new File(outFolder + "/common-rp-rn-apis.txt").readLines()
def comm_rp_vs_rn_sec = new File(outFolder + "/common-rp-rn-sec-apis.txt").readLines()
def comm_fn_vs_rn = new File(outFolder + "/common-fn-rn-apis.txt").readLines()
def comm_fn_vs_rn_sec = new File(outFolder + "/common-fn-rn-sec-apis.txt").readLines()
def comm_tp_vs_rn = new File(outFolder + "/common-tp-rn-apis.txt").readLines()
def comm_tp_vs_rn_sec = new File(outFolder + "/common-tp-rn-sec-apis.txt").readLines()
def comm_tp_vs_fn = new File(outFolder + "/common-tp-fn-apis.txt").readLines()
def comm_tp_vs_fn_sec = new File(outFolder + "/common-tp-fn-sec-apis.txt").readLines()
def comm_fnrn_vs_tprn = new File(outFolder + "/common-tp-fn-rn-apis.txt").readLines()

def commons = "common," + comm_rp_vs_rn.size() + "," + comm_rp_vs_rn_sec.size() + "," + comm_fn_vs_rn.size() + "," + comm_fn_vs_rn_sec.size() + "," + comm_tp_vs_rn.size() + "," + comm_tp_vs_rn_sec.size() + "," + comm_tp_vs_fn.size() + "," + comm_tp_vs_fn_sec.size() + "," + comm_fnrn_vs_tprn.size()

out.append(commons + "\n")

def rp_rp_vs_rn = new File(outFolder + "/only-rp-apis-rn.txt").readLines()
def rp_rp_vs_rn_sec = new File(outFolder + "/only-rp-sec-apis-rn.txt").readLines()
def fn_fn_vs_rn = new File(outFolder + "/only-fn-apis-rn.txt").readLines()
def fn_fn_vs_rn_sec = new File(outFolder + "/only-fn-sec-apis-rn.txt").readLines()
def tp_tp_vs_rn = new File(outFolder + "/only-tp-apis-rn.txt").readLines()
def tp_tp_vs_rn_sec = new File(outFolder + "/only-tp-sec-apis-rn.txt").readLines()
def tp_tp_vs_fn = new File(outFolder + "/only-tp-apis-fn.txt").readLines()
def tp_tp_vs_fn_sec = new File(outFolder + "/only-tp-sec-apis-fn.txt").readLines()
def fnrn_fnrn_vs_tprn = new File(outFolder + "/only-fn-rn-apis.txt").readLines()

def x = "unique x (x refers to the 1st elem of x vs y)," + rp_rp_vs_rn.size() + "," + rp_rp_vs_rn_sec.size() + "," + fn_fn_vs_rn.size() + "," + fn_fn_vs_rn_sec.size() + "," + tp_tp_vs_rn.size() + "," + tp_tp_vs_rn_sec.size() + "," + tp_tp_vs_fn.size() + "," + tp_tp_vs_fn_sec.size() + "," + fnrn_fnrn_vs_tprn.size()

out.append(x + "\n")

def rn_rp_vs_rn = new File(outFolder + "/only-rn-apis-rp.txt").readLines()
def rn_rp_vs_rn_sec = new File(outFolder + "/only-rn-sec-apis-rp.txt").readLines()
def rn_fn_vs_rn = new File(outFolder + "/only-rn-apis-fn.txt").readLines()
def rn_fn_vs_rn_sec = new File(outFolder + "/only-rn-sec-apis-fn.txt").readLines()
def rn_tp_vs_rn = new File(outFolder + "/only-rn-apis-tp.txt").readLines()
def rn_tp_vs_rn_sec = new File(outFolder + "/only-rn-sec-apis-tp.txt").readLines()
def fn_tp_vs_fn = new File(outFolder + "/only-fn-apis-tp.txt").readLines()
def fn_tp_vs_fn_sec = new File(outFolder + "/only-fn-sec-apis-tp.txt").readLines()
def tprn_fnrn_vs_tprn = new File(outFolder + "/only-tp-rn-apis.txt").readLines()

def y = "unique y (y refers to the 2nd elem of x vs y)," + rn_rp_vs_rn.size() + "," + rn_rp_vs_rn_sec.size() + "," + rn_fn_vs_rn.size() + "," + rn_fn_vs_rn_sec.size() + "," + rn_tp_vs_rn.size() + "," + rn_tp_vs_rn_sec.size() + "," + fn_tp_vs_fn.size() + "," + fn_tp_vs_fn_sec.size() + "," + tprn_fnrn_vs_tprn.size()

out.append(y)
