/*
 * Copyright (c) 2018, Kansas State University
 *
 * Licensed under BSD 3-clause License
 *
 * Author: Joydeep Mitra
 */

def cli = new CliBuilder(usage:"groovy diff.groovy")
cli.f1(longOpt:'file1', args:1, argName:'file1', required:true,
        'Name of api file1')
cli.f2(longOpt:'file2', args:1, argName:'file2', required:true,
        'Name of api file2')
cli.o1(longOpt:'outFile1', args:1, argName:'outFile1', required:true,
        'Name of output file with elements unique to file1')
cli.o2(longOpt:'outFile2', args:1, argName:'outFile2', required:true,
        'Name of output file with elements unique to file2')
cli.o3(longOpt:'outFile3', args:1, argName:'outFile3', required:true,
        'Name of output file with elements common to file and file2')

def options = cli.parse(args)
if (!options) {
    return
}

def file1 = new File(options.f1).readLines()
println "loaded " + options.f1
def file2 = new File(options.f2).readLines()
println "loaded " + options.f2

new File(options.o1).withPrintWriter{ def writer ->
  file1.findAll {
    (!(file2.contains(it)))
  }.sort().each{writer.println(it)}
}

new File(options.o2).withPrintWriter{ def writer ->
  file2.findAll {
    (!(file1.contains(it)))
  }.sort().each{writer.println(it)}
}

new File(options.o3).withPrintWriter{ def writer ->
  file1.findAll {
    ((file2.contains(it)))
  }.sort().each{writer.println(it)}
}
